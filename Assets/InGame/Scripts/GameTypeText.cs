﻿using UnityEngine;
using System.Collections;

public class GameTypeText : MonoBehaviour 
{
	GlobalVariables Var = GlobalVariables.GetInstance ();

	public Project Parent;

	public TextMesh Text;

	void Update () 
	{
		if(Parent.Genre == Project.Genres.None)
		{
			Text.text = "선택";
		}
		else if(Parent.Genre == Project.Genres.Violence)
		{
			Text.text = "폭력성";
		}
		else if(Parent.Genre == Project.Genres.Emotion)
		{
			Text.text = "감성";
		}
		else if(Parent.Genre == Project.Genres.Strategy)
		{
			Text.text = "전략";
		}
		else if(Parent.Genre == Project.Genres.Control)
		{
			Text.text = "컨트롤";
		}
		else if(Parent.Genre == Project.Genres.Liberty)
		{
			Text.text = "자유도";
		}
		else if(Parent.Genre == Project.Genres.Puzzle)
		{
			Text.text = "퍼즐";
		}
		else if(Parent.Genre == Project.Genres.Simplity)
		{
			Text.text = "단순성";
		}
		else if(Parent.Genre == Project.Genres.Story)
		{
			Text.text = "스토리";
		}
	}
}