using UnityEngine;
using System.Collections;

public class GameTypeCollider : MonoBehaviour 
{
	public GameTypeSelect Parent;

	public enum GameTypes
	{
		None,
		Violence,
		Emotion,
		Strategy,
		Control,
		Liberty,
		Puzzle,
		Simplity,
		Story,
	}

	public GameTypes GameType;

	void OnMouseDown()
	{
		if(GameType == GameTypes.None)
		{
			Parent.ParentPj.Genre = Project.Genres.None;
		}
		else if(GameType == GameTypes.Violence)
		{
			Parent.ParentPj.Genre = Project.Genres.Violence;
		}
		else if(GameType == GameTypes.Emotion)
		{
			Parent.ParentPj.Genre = Project.Genres.Emotion;
		}
		else if(GameType == GameTypes.Strategy)
		{
			Parent.ParentPj.Genre = Project.Genres.Strategy;
		}
		else if(GameType == GameTypes.Control)
		{
			Parent.ParentPj.Genre = Project.Genres.Control;
		}
		else if(GameType == GameTypes.Liberty)
		{
			Parent.ParentPj.Genre = Project.Genres.Liberty;
		}
		else if(GameType == GameTypes.Puzzle)
		{
			Parent.ParentPj.Genre = Project.Genres.Puzzle;
		}
		else if(GameType == GameTypes.Simplity)
		{
			Parent.ParentPj.Genre = Project.Genres.Simplity;
		}
		else if(GameType == GameTypes.Story)
		{
			Parent.ParentPj.Genre = Project.Genres.Story;
		}

		Destroy (Parent.gameObject);
	}
}