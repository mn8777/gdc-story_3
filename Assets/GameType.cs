﻿using UnityEngine;
using System.Collections;

public class GameType : MonoBehaviour 
{
	GlobalVariables Var = GlobalVariables.GetInstance ();

	public Project Parent;

	public GameTypeSelect SelectPopupPf;
	GameTypeSelect SelectPopup;

	public SpriteRenderer Renderer;

	void Update()
	{
		if(Parent.Genre == Project.Genres.None)
		{
			Renderer.enabled = true;
		}
		else
		{
			Renderer.enabled = false;
		}
	}

	void OnMouseDown()
	{
		SelectPopup = Instantiate (SelectPopupPf) as GameTypeSelect;
		SelectPopup.ParentPj = Parent;
	}
}